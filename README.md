# Hello! :earth_americas:


- :office: I'm currently working on Porto Alegre Bus Station - RS as Technical Support.

- :blue_heart: I'm passionate about tech, music, movie, dog, cat, games and of course my family!

- :pencil2: I'm currently learning JavaScript. 

[![linkedin](https://i.ibb.co/GCTcBjk/linkedin2.png)](https://www.linkedin.com/in/lucasrmagalhaes/)
[![spotify](https://i.ibb.co/mDJgncx/spotify.png)](https://open.spotify.com/user/ad75itafbn7w2633u16rwhb09)
[![facebook](https://i.ibb.co/VvDKYth/facebook.png)](https://www.facebook.com/darosamagalhaes)
[![instagram](https://i.ibb.co/m9d7KZ7/instagram2.png)](https://www.instagram.com/darosa.ti/?hl=pt-br)
[![twitter](https://i.ibb.co/sCjcWFV/twitter2.png)](https://twitter.com/lcs_maluro)